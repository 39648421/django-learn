from django.http import HttpResponse
from django.shortcuts import render_to_response
import datetime

def current_datetime(request):
	current_date = datetime.datetime.now()
	return render_to_response("current_datetime.html", locals())

def hours_ahead(request, offset):
	hour_offset = int(offset)
	next_time = datetime.datetime.now() + datetime.timedelta(hours=hour_offset)
	return render_to_response("future_time.html", locals())
